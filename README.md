# Cucumber Report Creater

这是一款可以集成到任何UI自动化测试框架的测试报告生成工具，界面样式完全照搬 Cucumber Report（可能是我比较喜欢绿色吧）。

### 如何使用
1、测试完成后，将你的报告数据组装成如下示例；  
2、读取 webReport.xml 文件；    
3、将重新组装的测试报告数据 json 序列化；    
4、用 json 序列化后的数据替换读取的 webReport.xml 文件中的  **#JSON_STRING__JSON_STRING#** 。

### 测试报告数据说明

```
{
	"cases": [{                                                                  --- 用例数组 
		"finishTime": "2018-09-20 14:11:01",                                 --- 用例完成时间  
		"caseName": "NG_Login_001 使用工号登录业务系统",                       --- 用例名称  
		"status": "success",                                                 --- 用例状态success-成功  fail、error-失败  
		"steps": [{                                                          --- 测试步骤数组 
			"mark": "",                                                  --- 备注，验证结果、出错信息、输入内容等  
			"status": true,                                              --- 步骤结果 true-正常 其他-失败  
			"stepId": "1",                                               --- 步骤id,需要保证全局唯一  
			"stepName": "打开开发测试环境地址",                            --- 步骤名称  
			"testTime": "2018-09-20 14:11:01",                           --- 测试时间  
			"screenshot": "snapshot\\NG_Validate_Transaction_Status.png" --- 该步骤的截图，可为空  
		}]  
	}],  
	"browserName": "Chrome",                                                     --- 运行浏览器类型  
	"testTime": "2018-09-20 14:10:58",                                           --- 测试时间  
	"endTime": "2018-09-20 14:14:03",                                            --- 结束时间  
	"totalCount": 3,                                                             --- 用例总数  
	"successCount": 2,                                                           --- 成功数量  
	"title": "UI自动化测试报告"                                                   --- 报告标题  
}
```

### 示例代码

##### Java

```
Gson gson = new GsonBuilder().disableHtmlEscaping().create();
String template = FileUtil.read("webReport.xml");
template = template.replaceFirst("#JSON_STRING__JSON_STRING#", gson.toJson(result));
output.write(template);
```

##### Python

```
file_object = open(path.join('./', 'webReport.xml'), encoding='utf-8')
try:
    file_context = file_object.read()
    file_context = file_context.replace('#JSON_STRING__JSON_STRING#', json.dumps(result))
    with open('xxxx.html', 'w', encoding='utf-8') as f:
      f.write(file_context)
finally:
    file_object.close()
```

##### Ruby

```
File.open("webReport.xml","r:utf-8") do |lines|
  buffer = lines.read.gsub('#JSON_STRING__JSON_STRING#', result.to_json)
  File.open("xxxxx.html","w"){|l|
    l.write(buffer)
  }
end
```



### 截图
![截图](https://images.gitee.com/uploads/images/2018/0921/105401_ee25e17b_431003.jpeg "TIM截图20180921103456.jpg")